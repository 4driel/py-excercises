from datetime import datetime

import argparse
import csv
import pytz
import sys

JMETER_LOGS = './Jmeter_log1.jtl'


def jmeter_logs_parser(log_file):
    with open(log_file, 'r') as csv_file:
        reader = csv.reader(csv_file)

        for index, row in enumerate(reader):
            if row[3] != '200' and index > 0:
                time_stamp = row[0]
                time_stamp = datetime.fromtimestamp(int(time_stamp)/1000).astimezone(pytz.timezone('US/Pacific'))
                time_stamp = time_stamp.strftime("%Y-%m-%d %H:%M:%S %Z")

                print(f"label: {row[2]}, resp_code: {row[3]}, resp_message: {row[4]}, fail_message: {row[8]}, timestamp: {time_stamp}")

    
def main(args=None):
    parser = argparse.ArgumentParser(
        description='A script to remove json elements from template file')
    parser.add_argument('-l', '--log_file', type=str, required=True, help='Jmeter log file to parse')
    params = parser.parse_args(sys.argv[1:] if not args else args)

    jmeter_logs_parser(params.log_file)


if __name__ == "__main__":
    try:
        main()
    except Exception:
        raise Exception("Failed to generate json payload")

"""EXAMPLE: python ./jmeter_logs_parser.py -l './Jmeter_log1.jtl'
"""
