import argparse
import json
import sys

JSON_TEMPLATE = './test_payload.json'


def generate_json(del_elem, output_file):
    new_data = {}
    with open(JSON_TEMPLATE) as jsonfile:
        data = json.load(jsonfile)
        new_data = traverse_json(data, del_elem)
    
    with open(output_file, "w") as new_file:
        new_file.write(json.dumps(new_data, indent=2))

def traverse_json(data, del_elem):
    if(isinstance(data,dict)):
        new_data = {}
        for key, val in data.items():
            if(isinstance(val,dict)):
                if(key != del_elem):
                    new_data[key] = traverse_json(val, del_elem)
                continue
            
            if(isinstance(val,list)):
                if(key != del_elem):
                    new_data[key] = traverse_json(val, del_elem)
                continue
            
            if(key != del_elem):
                new_data[key] = val

    elif(isinstance(data,list)):
        new_data = []
        for element in data:
            if(element != del_elem):
                new_data.append(element)
    return new_data
    
def main(args=None):
    parser = argparse.ArgumentParser(
        description='A script to remove json elements from template file')
    parser.add_argument('-d', '--del_elem', type=str, required=True, help='Json element to Delete')
    parser.add_argument('-o', '--output_file', type=str, required=True, help='Name of the newly generated json file')
    params = parser.parse_args(sys.argv[1:] if not args else args)

    generate_json(params.del_elem, params.output_file)


if __name__ == "__main__":
    try:
        main()
    except Exception:
        raise Exception("Failed to generate json payload")

"""EXAMPLE: python ./delete_json.py -n outParams -o test_payload2.json
            python ./delete_json.py -n dateeff -o test_payload2.json
            python ./delete_json.py -n planselect_1 -o test_payload2.json
"""
