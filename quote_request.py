from datetime import date, timedelta
from xml.etree import ElementTree

import argparse
import sys

QUOTE_TEMPLATE = './test_payload1.xml'


def gen_quote_request(depart_delta, ret_delta, quote_name):
    tree = ElementTree.parse(QUOTE_TEMPLATE)
    root = tree.getroot()

    today = date.today()
    depart = (today + timedelta(days=depart_delta)).strftime("%Y%m%d")
    ret = (today + timedelta(days=ret_delta)).strftime("%Y%m%d")

    for elem in root.iter('DEPART'):
        elem.text = depart

    for elem in root.iter('RETURN'):
        elem.text = ret

    tree.write(f'./{quote_name}.xml')

def main(args=None):
    parser = argparse.ArgumentParser(
        description='A script to generate quote requests based on DEPART & RETURN commitments')
    parser.add_argument('-d', '--depart', type=int, required=True, help='Departure commitment')
    parser.add_argument('-r', '--ret', type=int, required=True, help='Return guarantee')
    parser.add_argument('-n', '--quote_name', required=True, help='Quote name used to generate new file')
    params = parser.parse_args(sys.argv[1:] if not args else args)

    gen_quote_request(params.depart, params.ret, params.quote_name)


if __name__ == "__main__":
    try:
        main()
    except Exception:
        raise Exception("Failed to generate quote request")

# EXAMPLE: python ./quote_request.py -d 3 -r 5 -n test_payload2
